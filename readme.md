# Rick's Email Builder

An MJML email builder/workflow experiment

## How it works

Everything is built in template parts under src>mjml.
On index.mjml call in the template parts you wish to use or create new parts and call them in when needed.

**CSS goes in index.mjml:**

- Inline will be applied to any template parts called in and inlined to the classes you give the CSS and template parts
- Genral styling is found under inline in mj-style

### Setup

`yarn install`

- basic setup

### Commands

`yarn start`

- deletes old build
- starts browser sync so you can see changes live
- when changes happen it will build a new output
- output to dist as `index.html`
- `ctrl+c` to stop

`yarn build`

- Just builds based on input from index
