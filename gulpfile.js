const gulp = require('gulp')
const mjml = require('gulp-mjml')
const del = require('del')
const mjmlEngine = require('mjml')
const exec = require('gulp-exec')
const gutil = require('gulp-util')
const browserSync = require('browser-sync');
const server = browserSync.create();

function reload(done) {
  server.reload();
  done();
}

function serve(done) {
  server.init({
    server: {
      baseDir: 'dist'
    }
  });
  done();
}

const clean = () => del(['dist/index.html']);

const email = () => {
  return gulp.src('./src/mjml/index.mjml')
    .pipe(mjml(mjmlEngine, { minify: true }))
    .on('error', gutil.log)
    .pipe(gulp.dest('./dist'))
}

gulp.task(email)


const watch = () => gulp.watch('./src/mjml/*.mjml', gulp.series(clean, email, reload));

const dev = gulp.series(clean, email, serve, watch);

gulp.task('default', dev);